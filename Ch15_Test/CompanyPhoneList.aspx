﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CompanyPhoneList.aspx.cs" Inherits="CompanyPhoneList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="AreaCode" DataValueField="AreaCode">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DVConnectionString %>" SelectCommand="SELECT DISTINCT [AreaCode] FROM [PhoneNumber]"></asp:SqlDataSource>
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" Height="50px" Width="125px" DataKeyNames="PhoneNumberId">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                <EditRowStyle BackColor="#999999" />
                <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                <Fields>
                    <asp:TemplateField HeaderText="AreaCode" SortExpression="AreaCode">
                        <EditItemTemplate>
                            <asp:TextBox ID="EditAreaCode" runat="server" Text='<%# Bind("AreaCode") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AreaCode") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Prefix" HeaderText="Prefix" SortExpression="Prefix" />
                    <asp:BoundField DataField="Suffix" HeaderText="Suffix" SortExpression="Suffix" />
                    <asp:TemplateField HeaderText="PhoneNumberTypeID" SortExpression="PhoneNumberTypeID">
                        <EditItemTemplate>
                            <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource3" DataTextField="PhoneNumberTypeID" DataValueField="PhoneNumberTypeID">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DVConnectionString %>" SelectCommand="SELECT DISTINCT [PhoneNumberTypeID] FROM [PhoneNumber]"></asp:SqlDataSource>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PhoneNumberTypeID") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("PhoneNumberTypeID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Extension" HeaderText="Extension" SortExpression="Extension" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DVConnectionString %>" SelectCommand="SELECT [AreaCode], [Prefix], [Suffix], [PhoneNumberTypeID], [Extension], [PhoneNumberId] FROM [PhoneNumber] WHERE ([AreaCode] = @AreaCode)" DeleteCommand="DELETE FROM [PhoneNumber] WHERE [PhoneNumberId] = @PhoneNumberId" InsertCommand="INSERT INTO [PhoneNumber] ([AreaCode], [Prefix], [Suffix], [PhoneNumberTypeID], [Extension]) VALUES (@AreaCode, @Prefix, @Suffix, @PhoneNumberTypeID, @Extension)" UpdateCommand="UPDATE [PhoneNumber] SET [AreaCode] = @AreaCode, [Prefix] = @Prefix, [Suffix] = @Suffix, [PhoneNumberTypeID] = @PhoneNumberTypeID, [Extension] = @Extension WHERE [PhoneNumberId] = @PhoneNumberId">
                <DeleteParameters>
                    <asp:Parameter Name="PhoneNumberId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="AreaCode" Type="String" />
                    <asp:Parameter Name="Prefix" Type="String" />
                    <asp:Parameter Name="Suffix" Type="String" />
                    <asp:Parameter Name="PhoneNumberTypeID" Type="Int32" />
                    <asp:Parameter Name="Extension" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" Name="AreaCode" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="AreaCode" Type="String" />
                    <asp:Parameter Name="Prefix" Type="String" />
                    <asp:Parameter Name="Suffix" Type="String" />
                    <asp:Parameter Name="PhoneNumberTypeID" Type="Int32" />
                    <asp:Parameter Name="Extension" Type="String" />
                    <asp:Parameter Name="PhoneNumberId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
