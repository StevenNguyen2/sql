﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Insert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlDataSource1.InsertParameters["FirstName"].DefaultValue = txtFirstName.Text;
        SqlDataSource1.InsertParameters["MiddleName"].DefaultValue = txtMiddleName.Text;
        SqlDataSource1.InsertParameters["LastName"].DefaultValue = txtLastName.Text;
        SqlDataSource1.InsertParameters["Email"].DefaultValue = txtEmail.Text;
        SqlDataSource1.InsertParameters["DOB"].DefaultValue = txtDOB.Text;
        SqlDataSource1.InsertParameters["SSN"].DefaultValue = txtSSN.Text;

        try
        {
            SqlDataSource1.Insert();
            lblResult.Text = "Works";
        }
        catch (Exception ex)
        {
            lblResult.Text = ex.Message;
        }
    }
}