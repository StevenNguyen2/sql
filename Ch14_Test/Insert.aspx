﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Insert.aspx.cs" Inherits="Insert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HOT3ConnectionString %>" InsertCommand="spInsert" InsertCommandType="StoredProcedure" SelectCommand="spGetData" SelectCommandType="StoredProcedure">
                <InsertParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="MiddleName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="DOB" Type="String" />
                    <asp:Parameter Name="SSN" Type="String" />
                </InsertParameters>
            </asp:SqlDataSource>
            <span>
                First Name:
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            </span>
            <br />
            <span>
                Middle Name:
                <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
            </span>
            <br />
            <span>
                Last Name:
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </span>
            <br />
            <span>
                Email:
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </span>
            <br />
            <span>
                 DOB:
                <asp:TextBox ID="txtDOB" runat="server"></asp:TextBox>
            </span>
            <br />
            <span>
                SSN:
                <asp:TextBox ID="txtSSN" runat="server"></asp:TextBox>
            </span>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Button" OnClick="btnSubmit_Click" />
            <asp:Label ID="lblResult" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
