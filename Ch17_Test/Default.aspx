﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="PhoneNumber" InsertMethod="InsertPhoneNumber" SelectMethod="GetPhoneNumber" TypeName="PhoneNumberDB" ConflictDetection="CompareAllValues" DeleteMethod="DeletePhoneNumber" OldValuesParameterFormatString="original_{0}" UpdateMethod="UpdateCategory">
                <UpdateParameters>
                    <asp:Parameter Name="original_Phone" Type="Object" />
                    <asp:Parameter Name="phone" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="PhoneNumberID" HeaderText="PhoneNumberID" SortExpression="PhoneNumberID" />
                    <asp:BoundField DataField="PhoneNumberTypeID" HeaderText="PhoneNumberTypeID" SortExpression="PhoneNumberTypeID" />
                    <asp:BoundField DataField="AreaCode" HeaderText="AreaCode" SortExpression="AreaCode" />
                    <asp:BoundField DataField="Prefix" HeaderText="Prefix" SortExpression="Prefix" />
                    <asp:BoundField DataField="Suffix" HeaderText="Suffix" SortExpression="Suffix" />
                    <asp:BoundField DataField="Extension" HeaderText="Extension" SortExpression="Extension" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="ObjectDataSource1" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="PhoneNumberID" HeaderText="PhoneNumberID" SortExpression="PhoneNumberID" />
                    <asp:BoundField DataField="PhoneNumberTypeID" HeaderText="PhoneNumberTypeID" SortExpression="PhoneNumberTypeID" />
                    <asp:BoundField DataField="AreaCode" HeaderText="AreaCode" SortExpression="AreaCode" />
                    <asp:BoundField DataField="Prefix" HeaderText="Prefix" SortExpression="Prefix" />
                    <asp:BoundField DataField="Suffix" HeaderText="Suffix" SortExpression="Suffix" />
                    <asp:BoundField DataField="Extension" HeaderText="Extension" SortExpression="Extension" />
                    <asp:CommandField ShowDeleteButton="True" ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
        </div>
    </form>
</body>
</html>
