﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PhoneNumber
/// </summary>
public class PhoneNumber
{
    public PhoneNumber()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string PhoneNumberID { get; set; }
    public string PhoneNumberTypeID { get; set; }
    public string AreaCode { get; set; }
    public string Prefix { get; set; }
    public string Suffix { get; set; }
    public string Extension { get; set; }
}