﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PhoneNumberDB
/// </summary>
public class PhoneNumberDB
{
    public PhoneNumberDB()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [DataObjectMethod(DataObjectMethodType.Select)]

    public static List<PhoneNumber> GetPhoneNumber()
    {
        List<PhoneNumber> phoneNumberList = new List<PhoneNumber>();

        string sel = "select PhoneNumberID, PhoneNumberTypeID, AreaCode, Prefix, Suffix, Extension "
            + "from PhoneNumber order by PhoneNumberID";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sel, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while(dr.Read())
                {
                    PhoneNumber phone;
                    phone = new PhoneNumber();
                    phone.PhoneNumberID = dr["PhoneNumberID"].ToString();
                    phone.PhoneNumberTypeID = dr["PhoneNumberTypeID"].ToString();
                    phone.AreaCode = dr["AreaCode"].ToString();
                    phone.Prefix = dr["Prefix"].ToString();
                    phone.Suffix = dr["Suffix"].ToString();
                    phone.Extension = dr["Extension"].ToString();
                    phoneNumberList.Add(phone);
                }
                dr.Close();
            }
        }
        return phoneNumberList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]

    public static void InsertPhoneNumber(PhoneNumber phone)
    {
        string ins = "INSERT into PhoneNumber "
            + "(PhoneNumberTypeID, AreaCode, Prefix, Suffix, Extension) "
            + "VALUES (@PhoneNumberTypeID, @AreaCode, @Prefix, @Suffix, @Extension)";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(ins, con))
            {
                //cmd.Parameters.AddWithValue("PhoneNumberID", phone.PhoneNumberID);
                cmd.Parameters.AddWithValue("PhoneNumberTypeID", phone.PhoneNumberTypeID);
                cmd.Parameters.AddWithValue("AreaCode", phone.AreaCode);
                cmd.Parameters.AddWithValue("Prefix", phone.Prefix);
                cmd.Parameters.AddWithValue("Suffix", phone.Suffix);
                cmd.Parameters.AddWithValue("Extension", phone.Extension);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]

    public static int DeletePhoneNumber(PhoneNumber phone)
    {
        int deleteCount = 0;

        string sql = "DELETE FROM PhoneNumber "
            + "WHERE PhoneNumberID = @PhoneNumberID "
            + "AND PhoneNumberTypeID = @PhoneNumberTypeID "
            + "AND AreaCode = @AreaCode "
            + "AND Prefix = @Prefix "
            + "AND Suffix = @Suffix "
            + "AND Extension = @Extension";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("PhoneNumberID", phone.PhoneNumberID);
                cmd.Parameters.AddWithValue("PhoneNumberTypeID", phone.PhoneNumberTypeID);
                cmd.Parameters.AddWithValue("AreaCode", phone.AreaCode);
                cmd.Parameters.AddWithValue("Prefix", phone.Prefix);
                cmd.Parameters.AddWithValue("Suffix", phone.Suffix);
                cmd.Parameters.AddWithValue("Extension", phone.Extension);
                con.Open();
                deleteCount = cmd.ExecuteNonQuery();
            }
        }
        return deleteCount;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]

    public static int UpdateCategory(PhoneNumber original_Phone, PhoneNumber phone)
    {
        int updateCount = 0;

        string sql = "UPDATE PhoneNumber "
            + "SET PhoneNumberTypeID = @PhoneNumberTypeID, "
            + "AreaCode = @AreaCode, "
            + "Prefix = @Prefix, "
            + "Suffix = @Suffix, "
            + "Extension = @Extension "
            + "WHERE PhoneNumberID = @original_PhoneNumberID "
            + "AND PhoneNumberTypeID = @original_PhoneNumberTypeID "
            + "AND AreaCode = @original_AreaCode "
            + "AND Prefix = @original_Prefix "
            + "AND Suffix = @original_Suffix "
            + "AND Extension = @original_Extension";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("PhoneNumberTypeID", phone.PhoneNumberTypeID);
                cmd.Parameters.AddWithValue("AreaCode", phone.AreaCode);
                cmd.Parameters.AddWithValue("Prefix", phone.Prefix);
                cmd.Parameters.AddWithValue("Suffix", phone.Suffix);
                cmd.Parameters.AddWithValue("Extension", phone.Extension);
                cmd.Parameters.AddWithValue("original_PhoneNumberID", original_Phone.PhoneNumberID);
                cmd.Parameters.AddWithValue("original_PhoneNumberTypeID", original_Phone.PhoneNumberTypeID);
                cmd.Parameters.AddWithValue("original_AreaCode", original_Phone.AreaCode);
                cmd.Parameters.AddWithValue("original_Prefix", original_Phone.Prefix);
                cmd.Parameters.AddWithValue("original_Suffix", original_Phone.Suffix);
                cmd.Parameters.AddWithValue("original_Extension", original_Phone.Extension);
                con.Open();
                updateCount = cmd.ExecuteNonQuery();
            }
        }
        return updateCount;
    }

    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["ODSConnectionString"].ConnectionString;
    }
}