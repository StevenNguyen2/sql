﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CategoryDB
/// </summary>
/// 

    [DataObject(true)]
public class CategoryDB
{
    public CategoryDB()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [DataObjectMethod(DataObjectMethodType.Select)]

    public static List<Category> GetCategories()
    {
        List<Category> categoryList = new List<Category>();

        string sel = "select categoryID, shortname, longname "
            + "from categories order by shortname";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sel, con))
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Category category;
                    category = new Category();
                    category.CategoryID = dr["CategoryID"].ToString();
                    category.ShortName = dr["ShortName"].ToString();
                    category.LongName = dr["LongName"].ToString();
                    categoryList.Add(category);
                }
                dr.Close();
            }
        }
        return categoryList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]

    public static void InsertCategory(Category category)
    {
        string ins = "INSERT into Categories "
            + "(CategoryID, ShortName, LongName) "
            + "VALUES (@CategoryID, @ShortName, @LongName)";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(ins, con))
            {
                cmd.Parameters.AddWithValue("CategoryID", category.CategoryID);
                cmd.Parameters.AddWithValue("ShortName", category.ShortName);
                cmd.Parameters.AddWithValue("LongName", category.LongName);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]

    public static int DeleteCategory(Category category)
    {
        int deleteCount = 0;

        string sql = "DELETE FROM Categories "
            + "WHERE CategoryID = @CategoryID "
            + "AND ShortName = @ShortName "
            + "AND LongName = @LongName";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("CategoryID", category.CategoryID);
                cmd.Parameters.AddWithValue("ShortName", category.ShortName);
                cmd.Parameters.AddWithValue("LongName", category.LongName);
                con.Open();
                deleteCount = cmd.ExecuteNonQuery();
            }
        }
        return deleteCount;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]

    public static int UpdateCategory(Category original_Category, Category category)
    {
        int updateCount = 0;

        string sql = "UPDATE Categories "
            + "SET ShortName = @ShortName, "
            + "LongName = @LongName "
            + "WHERE CategoryID = @original_CategoryID "
            + "AND ShortName = @original_ShortName "
            + "AND LongName = @original_LongName";

        using (SqlConnection con = new SqlConnection(GetConnectionString()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("ShortName", category.ShortName);
                cmd.Parameters.AddWithValue("LongName", category.LongName);
                cmd.Parameters.AddWithValue("original_CategoryID", original_Category.CategoryID);
                cmd.Parameters.AddWithValue("original_ShortName", original_Category.ShortName);
                cmd.Parameters.AddWithValue("original_LongName", original_Category.LongName);
                con.Open();
                updateCount = cmd.ExecuteNonQuery();
            }
        }
        return updateCount;
    }

    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["HalloweenConnectionString"].ConnectionString;
    }
}