﻿public class Category
{
    public Category()
    {

    }

    public string CategoryID { get; set; }

    public string ShortName { get; set; }

    public string LongName { get; set; }
}