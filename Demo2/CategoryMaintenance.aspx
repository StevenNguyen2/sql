﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CategoryMaintenance.aspx.cs" Inherits="CategoryMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetCategories" TypeName="CategoryDB" ConflictDetection="CompareAllValues" DataObjectTypeName="Category" DeleteMethod="DeleteCategory" InsertMethod="InsertCategory" UpdateMethod="UpdateCategory">
                <UpdateParameters>
                    <asp:Parameter Name="original_Category" Type="Object" />
                    <asp:Parameter Name="category" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" SortExpression="CategoryID" />
                    <asp:BoundField DataField="ShortName" HeaderText="ShortName" SortExpression="ShortName" />
                    <asp:BoundField DataField="LongName" HeaderText="LongName" SortExpression="LongName" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:Label ID="Label1" runat="server" Text="To create a new category, enter the category info and click New then Insert"></asp:Label>
            <br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="ObjectDataSource1" Height="50px" style="margin-right: 0px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" SortExpression="CategoryID" />
                    <asp:BoundField DataField="ShortName" HeaderText="ShortName" SortExpression="ShortName" />
                    <asp:BoundField DataField="LongName" HeaderText="LongName" SortExpression="LongName" />
                    <asp:CommandField ShowInsertButton="True" />
                </Fields>
            </asp:DetailsView>
        </div>
    </form>
</body>
</html>
