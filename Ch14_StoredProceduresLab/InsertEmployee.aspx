﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsertEmployee.aspx.cs" Inherits="InsertEmployee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Stored Procedures and SQL Data Source</h1>
            <h2>CRUD Operations</h2>
            <h3>Inserting Data into a table</h3>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RankenConnectionString %>" InsertCommand="spInsertEmployee" InsertCommandType="StoredProcedure" SelectCommand="spGetDepartmentData" SelectCommandType="StoredProcedure">
                <InsertParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="DeptId" Type="Int32" />
                </InsertParameters>
            </asp:SqlDataSource>
            <span>Select Employee Department: </span>
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="DepartmentName" DataValueField="DepartmentId"></asp:DropDownList>
            <br />
            <span>First Name: </span><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            <br />
            <span>Last Name: </span><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Create Employee" OnClick="btnSubmit_Click" />
            <br />
            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
