﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceLineItems.aspx.cs" Inherits="InvoiceLineItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Choose an Invoice: "></asp:Label>
            <asp:DropDownList ID="ddlInvoice" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="InvoiceNumber" DataValueField="InvoiceNumber">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HalloweenConnectionString %>" SelectCommand="SELECT [InvoiceNumber] FROM [Invoices] WHERE ([Total] &gt; @Total)">
                <SelectParameters>
                    <asp:Parameter DefaultValue="300" Name="Total" Type="Decimal" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:HalloweenConnectionString %>" SelectCommand="SELECT [ProductID], [UnitPrice], [Quantity], [Extension] FROM [LineItems] WHERE ([InvoiceNumber] = @InvoiceNumber)">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlInvoice" Name="InvoiceNumber" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DataList ID="dlLineItems" runat="server" DataSourceID="SqlDataSource2">
            <ItemTemplate>
                ProductID:
                <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' />
                <br />
                UnitPrice:
                <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />
                <br />
                Quantity:
                <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />
                <br />
                Extension:
                <asp:Label ID="ExtensionLabel" runat="server" Text='<%# Eval("Extension") %>' />
                <br />
<br />
            </ItemTemplate>
        </asp:DataList>
    </form>
</body>
</html>
