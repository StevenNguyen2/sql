﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductDB
/// </summary>
public class ProductDB
{
    public ProductDB()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Select method for the Object Data Source to call
    public static IEnumerable GetAllCategories()
    {
        SqlConnection con = new SqlConnection(GetConnectionString());

        string sel = "select categoryID, longname " + "from categories order by longname";

        SqlCommand cmd = new SqlCommand(sel, con);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        return dr;
    }

    private static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["HalloweenConnectionString"].ConnectionString;
    }

    public static IEnumerable GetProductsByCategory(String CategoryID)
    {
        SqlConnection con = new SqlConnection(GetConnectionString());
        string sel = "select ProductID, Name, "
            + "Format(UnitPrice,'C', 'en-us') as Price, Onhand "
            + "FROM Products "
            + "WHERE CategoryID = @CategoryID "
            + "Order by ProductID";

        SqlCommand cmd = new SqlCommand(sel, con);
        cmd.Parameters.AddWithValue("CategoryID", CategoryID);
        con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        return dr;
    }
}