﻿/* 14 */
SELECT CompanyName
FROM SalesLT.Customer
JOIN SalesLT.CustomerAddress ON (SalesLT.Customer.CustomerID = SalesLt.CustomerAddress.CustomerID)
JOIN SalesLT.Address ON (SalesLT.CustomerAddress.AddressID = SalesLT.Address.AddressID)
WHERE City='Dallas';

/* 15 */
SELECT SUM(OrderQty) AS 'Items more than $1000'
FROM SalesLT.Product
JOIN SalesLT.SalesOrderDetail ON (SalesLT.Product.ProductID = SalesLT.SalesOrderDetail.ProductID)
WHERE ListPrice > 1000;

/* 16 */
SELECT CompanyName
FROM SalesLT.Customer
JOIN SalesLT.SalesOrderHeader ON (SalesLT.Customer.CustomerID = SalesLT.SalesOrderHeader.CustomerID)
GROUP BY CompanyName
HAVING SUM(Subtotal + TaxAMT + Freight) > 100000;

/* 17 */
SELECT COUNT(*)
FROM SalesLT.Customer
JOIN SalesLT.SalesOrderHeader ON (SalesLT.Customer.CustomerID = SalesLt.SalesOrderHeader.CustomerID)
JOIN SalesLt.SalesOrderDetail ON (SalesLT.SalesOrderHeader.SalesOrderID = SalesLt.SalesOrderDetail.SalesOrderID)
JOIN SalesLT.Product ON (SalesLT.SalesOrderDetail.ProductID = SalesLT.Product.ProductID)
WHERE CompanyName = 'Riding Cycles' AND Name = 'Racing Socks, L';